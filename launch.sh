#!/bin/sh
#
#git pull #origin master
##git checkout master
#./gradlew run --args='config debug_configs/sheeple_debug.json' & disown
##result=$? #returns the exit code of the last command
#
#if [ $? -eq 0 ]
#then
#  exit 0
#else
#  exit 1
#fi
while :
  do
    if test -f Sheeple-latest.jar; then
      rm Sheeple-current.jar
      mv Sheeple-latest.jar Sheeple-current.jar
    fi

    if test -f Sheeple-current.jar; then
      java -jar Sheeple-current.jar "$1" "$2"
      if [ "$?" -ne 2 ] #everything other that exit code 1 will result in a shutdown. otherwise relaunch
        then
          echo "java -jar didnt signal a relaunch. Ending..."
          break 2
      fi
    else
      echo "Can't find Sheeple-current.jar"
      break 2
    fi

  done