package bot;

import nl.altindag.ssl.SSLFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import java.io.IOException;

public class SecurityUtils {

    private static final String[] protocols = new String[] {"TLSv1.3"};
    private static final String[] cipher_suites = new String[] {"TLS_AES_128_GCM_SHA256"};
    private static final String trustStorePath = "/truststore.jts";
    private static final String trustStorePwd = "sheeple";
    private static SecurityUtils instance;
    private static SSLContext context;

    private SecurityUtils() {}

    public static SecurityUtils get() {
        if(instance == null) {
            instance = new SecurityUtils();
        }
        return instance;
    }

    public SSLSocket createSocket(String host, int port) throws IOException {

        SSLContext sslContext = getSSLContext();
        SSLSocket socket = (SSLSocket) sslContext.getSocketFactory()
                .createSocket(host, port);
        socket.setEnabledProtocols(protocols);
        socket.setEnabledCipherSuites(cipher_suites);

        return socket;
    }

    public SSLContext getSSLContext() {
        if(context == null) {
            //create custom SSLContext with our own trusted certificate and the default ones
            SSLFactory sslFactory = SSLFactory.builder()
                    .withDefaultTrustMaterial()
                    .withTrustMaterial(SecurityUtils.class.getResourceAsStream(trustStorePath), trustStorePwd.toCharArray())
                    .build();

            context = sslFactory.getSslContext();
        }

        return context;
    }
}
