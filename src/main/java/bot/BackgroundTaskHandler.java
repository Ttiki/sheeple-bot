package bot;

import java.util.Timer;
import java.util.TimerTask;

public class BackgroundTaskHandler {

    /**
     * Go through tasks list every X seconds
     */
    public static final int INTERVALL_SEC = 15 * 60;
//            public static final int INTERVALL_SEC = 60;


    private static BackgroundTaskHandler instance;

    Timer tasks;

    private BackgroundTaskHandler() {
        tasks = new Timer("background tasks");
    }

    /**
     * Add a task to the list of background tasks. These will be processed sequentially
     * @param r
     */
    public void addBackgroundTask(TimerTask r, long delay, long period) {
        synchronized (tasks) {
            tasks.schedule(r, delay, period);
        }
    }

    /**
     * Get singleton
     * @return
     */
    public static BackgroundTaskHandler get() {
        if(instance == null) {
            instance = new BackgroundTaskHandler();
        }
        return instance;
    }

}
