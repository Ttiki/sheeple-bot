package bot.logging;

import lombok.NonNull;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class LogToDiscordChannelHandler extends Handler {

    private String logChannelId;
    private JDA api;

    public LogToDiscordChannelHandler(@NonNull JDA api, @NonNull String logChannelId) {
        this.api = api;
        this.logChannelId = logChannelId;
    }

    @Override
    public void publish(LogRecord logRecord) {
        if(!isLoggable(logRecord))
            return;

        try {
            String log = getFormatter().format(logRecord);
            TextChannel channel = api.getTextChannelById(logChannelId);

            if(channel == null) {
                throw new Exception("Channel not found! logChannelId: " + logChannelId);
            }

            api.getTextChannelById(logChannelId).sendMessage(log).queue();
            if(logRecord.getThrown() != null) {
                StringBuilder builder = new StringBuilder(String
                        .format("%s: %s%n", logRecord.getThrown().getCause(),logRecord.getThrown().getMessage())
                );

                for(StackTraceElement entry : logRecord.getThrown().getStackTrace()) {
                    if(builder.length() + entry.toString().length() < 2000) {
                        builder.append(entry).append("\n");
                    }
                    else {
                        api.getTextChannelById(logChannelId).sendMessage(builder).queue();
                        builder = new StringBuilder(entry.toString());
                    }
                }
                api.getTextChannelById(logChannelId).sendMessage(builder).queue();
            }
        } catch (Exception e) {
            //In the unexpected case that something goes wrong we will write to err instead and leave it at that
            System.err.println("Couldnt send log message to discord: " + e.getMessage());
        }
    }

    @Override
    public void flush() {

    }

    @Override
    public void close() throws SecurityException {

    }
}
