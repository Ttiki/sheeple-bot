package bot;

import bot.settings.Settings;

import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MessageDeleteErrorHandler implements Consumer<Throwable> {
    @Override
    public void accept(Throwable error) {
        Logger.getLogger(Settings.LOGGER_NAME).log(Level.INFO, () -> error.getMessage() + ". Couldn't delete.");
    }
}
