package bot;

import bot.commands.Command;
import bot.commands.CommandCategories;
import bot.commands.RequirementsCheckResult;
import bot.logging.LogFormatter;
import bot.logging.LogToDiscordChannelHandler;
import bot.settings.Settings;
import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * This is the bot.PiBot for SheepIt (https://sheepit-renderfarm.com)
 * Its original goal was to be a day counter but it got extended to be a FAQ and guild management bot.
 * The bot gets its configuration from a file called sheepit.json in the same directory as the jar.
 * This is the minimum config for the bot to work:
 * <code>
 *  {
 *   "discordToken" : "your token here"
 * }
 * </code>
 * The bot will add various default commands along with some parameters to adjust the design of its messages if it cant find these values.
 * For more information on them see here: https://gist.github.com/zekroTJA/c8ed671204dafbbdf89c36fc3a1827e1
 * If you dont provide a config file the bot will fail but still create one for you that you can add your bot token to.
 * You can create new commands by adding them to the commands array in the config file. The standard json-syntax applies.
 * each command entry consists of five fields:
 * name : The bot command to trigger a certain action
 * output : The expected message for the command. Be aware that you might have to escape markdown characters if you dont
 *          want them to be interpreted
 * requiredPermission : the permission a user must have to trigger that command.
 *          The most basic one is {@link Permission#MESSAGE_READ}
 * description: a short description for !sheeple-help
 * category : for possible values see {@link CommandCategories}
 *
 * The config also includes these values:
 * startDate        The last time the pi-counter was reset
 * piHighscore      the longest streak between two resets
 * resetCounter     the number of times the !sheeple-reset command has been called
 * sheepitAdminRoleIDs  people with any of these roles are allowed to call any command. Stored in {@link Settings#getSuperRoleIDs()}
 * roleChannelID    the channel id for the role-request channel
 * assignableRoleIDs array with role ids that can be requested and assigned by the bot with !role-join/leave
 * Restart the client after you have made changes to the config
 */
public class SheepleBot extends ListenerAdapter {

    private static JDA api;
    private static Logger log;
    private static Settings settings;
    @Getter
    private static BackgroundTaskHandler backgroundTaskHandler;

    private MessageDeleteErrorHandler errorHandler;

    public SheepleBot() {
        this.errorHandler = new MessageDeleteErrorHandler();
    }

    public static void main(String[] args) {

        backgroundTaskHandler = BackgroundTaskHandler.get();
//        backgroundTaskHandler.addBackgroundTask(new UpdateTask());

        log = Logger.getLogger(Settings.LOGGER_NAME);
//        configureLogger(log);

        if(args.length > 0) {
            log.log(Level.INFO, () -> String.format("Received these launch arguments: %s", String.join(" ", args)));
            if(args.length > 1 && (args[0].equals("-config") || args[0].equals("config"))) {
                Settings.setConfigFile(args[1]);
            }
        }

        //load bot.settings
        settings = Settings.getInstance();
        if(settings == null) {
            log.log(Level.SEVERE, () -> "Could not read bot token! Make sure to set it in the config file like this: " +
                    "\"discordToken\" : \"xxxxxxxxxxx\"");
            System.exit(-1);
        }

        //build the bot
        Collection<GatewayIntent> intents = new ArrayList<>();
        intents.add(GatewayIntent.GUILD_MESSAGES);
        intents.add(GatewayIntent.GUILD_MEMBERS);
        JDABuilder builder = null;
        builder = JDABuilder.create(settings.getDiscordToken(), intents);
        builder.setActivity(Activity.listening("/sheeple-help"));
        builder.addEventListeners(new SheepleBot(), new JoinListener(), new SpamDetection());
        builder.disableCache(CacheFlag.ACTIVITY, CacheFlag.VOICE_STATE, CacheFlag.EMOTE, CacheFlag.CLIENT_STATUS, CacheFlag.ONLINE_STATUS);
      //  builder.setMemberCachePolicy(MemberCachePolicy.ALL);

//        builder.setChunkingFilter((guildId -> guildId == Long.valueOf(settings.getGuildID())));
//        builder.setChunkingFilter((guildId -> guildId == Long.parseLong(settings.getGuildID())));
       // builder.setChunkingFilter(ChunkingFilter.ALL);
        try {
            api = builder.build().awaitReady();
            log.log(Level.INFO, () -> String.format("Bot v%s launched", Settings.BOT_VERSION));
            log.log(Level.INFO, () -> String.format("Invite for this bot: %s", api.getInviteUrl(Permission.MANAGE_ROLES,
                    Permission.MESSAGE_READ,
                    Permission.VIEW_CHANNEL,
                    Permission.MESSAGE_HISTORY,
                    Permission.MANAGE_CHANNEL)));

        } catch (LoginException | InterruptedException | ErrorResponseException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception on trying to connect to Discord: ");
            System.exit(0);
        }
        configureLogger(log);   //from here on out log messages are also sent to discord

        for(Guild guild : api.getGuilds()) {
            for(Command cmd : settings.getCommands().values()) {
                cmd.registerCommand(guild);
            }
        }

//        ShutdownHook shutdownHook = new ShutdownHook(api, settings.getCommands().values());
//        Runtime.getRuntime().addShutdownHook(shutdownHook);
//        Signal.handle(new Signal("INT"), shutdownHook);

        log.log(Level.INFO, () -> String.format("Starting with these commands: %s", String.join(", ", settings.getCommands().keySet())));
        log.log(Level.INFO, () -> String.format("Requestable roles: %s", String.join(", ", settings.getAssignableRoles())));
        log.log(Level.INFO, () -> String.format("sheepitAdminRoleIds: %s", String.join(", ", settings.getSuperRoleIDs())));
        log.log(Level.INFO, () -> String.format("startDate: %s", settings.getStartDate()));
    }

    @Override
    public void onSlashCommand(SlashCommandEvent event) {

        if (ignoreMember(event)) return;

        Command command = settings.getCommands().get(event.getName());
        if(command == null) {
            event.reply("Command not found").setEphemeral(true).queue();
            return;
        }

        try {
//            event.deferReply().queue(interactionHook -> {}, interactionHook ->
//                    log.log(Level.WARNING, () -> String.format("%s called %s: Event expired", event.getMember().getUser().getAsTag(), event.getName())));

            RequirementsCheckResult reqsFullfilled = command.meetsRequirements(event);

            log.log(Level.INFO, () -> {
                return String.format("%s called %s in %s. Arguments: %s. Requirements check: %s",
                        event.getMember().getUser().getAsTag(),
                        event.getName(),
         //               event.getChannel().getName(),
                        event.getChannel(),
                        getSlashArgumentsString(event),
                        reqsFullfilled.name());
            });

            switch (reqsFullfilled) {

                case NO_PERMISSION: {
                    StringBuilder errorMsg = new StringBuilder(String.format("You need the %s permission to do that", command.getRequiredPermission()));
                    MessageEmbed msg = Settings.getInstance().createMessage(errorMsg.toString());

                    event.replyEmbeds(msg).queue(
                            m -> m.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                    null, errorHandler)
                    );
                    break;
                }

                case MUST_BE_SHEEPMIN: {
                    StringBuilder errorMsg = new StringBuilder(String.format("You must have the %s permission to do that or have one of the following roles: ",
                            command.getRequiredPermission()));
                    for (String id : Settings.getInstance().getSuperRoleIDs()) {
                        errorMsg.append(event.getGuild().getRoleById(id).getName()).append(" ");
                    }
                    event.replyEmbeds(settings.createMessage(errorMsg.toString())).queue(
                            m -> m.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                    null, errorHandler)
                    );
                    break;
                }

                case WRONG_CHANNEL: {
                    MessageEmbed errorMsg = Settings.getInstance().createMessage(String.format("Use the <#%s> channel", Settings.getInstance().getRoleChannelID()));
                    event.replyEmbeds(errorMsg).queue(
                            m -> m.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                    null, errorHandler)
                    );
                    break;
                }

                case ERROR: {
                    MessageEmbed errorMsg = Settings.getInstance().createMessage("An error occurred while checking for requirements");
                    log.log(Level.SEVERE, () -> String.format("%s called %s in %s: %s", event.getMember(), event.getName(),
                            event.getTextChannel().getName(), errorMsg));
                    event.replyEmbeds(errorMsg).queue();
                    break;
                }

                case IGNORE:
                    break;

                case INVALID_ARGUMENT: {
                    MessageEmbed errorMsg = Settings.getInstance().createMessage("Invalid argument");
                    event.replyEmbeds(errorMsg).queue(
                            m -> m.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                    null, errorHandler)
                    );
                    break;
                }

                case OK:
                    synchronized (settings.getGlobalLock()) {
                        Optional<MessageEmbed> response = command.handle(event);

                        if(event.getChannel().getId().equals(settings.getRoleChannelID()) && response.isPresent() &&!event.getHook().isExpired()) {
                            event.replyEmbeds(response.get()).setEphemeral(command.isEphemeral()).queue(
                                    msg -> msg.deleteOriginal().queueAfter(settings.getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                            null, errorHandler)
                            );
                        }
                        else if (response.isPresent() && !event.getHook().isExpired()) {
                            if(event.isAcknowledged()) {
                                event.getHook().sendMessageEmbeds(response.get()).queue();
                            }
                            else {
                                event.replyEmbeds(response.get()).queue();
                            }
                        }
                    }
                    break;
            }
        } catch (IllegalStateException e) {
            log.log(Level.SEVERE, String.format("%s on calling %s", e.getMessage(), event.getName()));
        } catch (InsufficientPermissionException e) {
            event.replyEmbeds(settings.createMessage("Couldn't delete a message because of lacking permission: "
                    + e.getMessage())).queue();
        } catch (ErrorResponseException e) {
            String msg = String.format("%s on calling %s", e.getMessage(), event.getName());
            log.log(Level.SEVERE, msg);
            event.getHook().sendMessageEmbeds(settings.createMessage(msg + " Interaction might have taken too long.")).queue();
        }
    }

    private boolean ignoreMember(SlashCommandEvent event) {
        try {
            boolean isTwit = callerIsTwit(event.getMember());
            boolean isMuted = event.getMember().getRoles().contains(
                    event.getGuild().getRoleById(settings.getMutedRoleID())
            );

            if (!event.isFromGuild()
                    || event.getMember() == null
                    || isTwit
                    || isMuted) {
                event.reply("Request ignored").setEphemeral(true).queue();
                log.log(Level.INFO, () -> String.format("%s called %s but was ignored (Caller is twit: %s, is guild message: %s)",
                        event.getMember().getUser().getAsTag(), event.getName(), isTwit, event.isFromGuild()));
                return true;
            }
        } catch (NullPointerException e) {
            log.log(Level.WARNING, e, () -> "Exception on trying to process SlashEvent");
            return true;
        }
        return false;
    }

    private boolean callerIsTwit(Member member) {
        LocalTime time = settings.getIgnoredMembers().get(member.getId());
        if(settings.getIgnoredMembers().size() > 0 && time != null) {
            if(time.plus(Settings.TWIT_IGNORE_TIME_MINUTES).isBefore(LocalTime.now())) { //time-out is over
                settings.getIgnoredMembers().remove(member.getId());
            }
            else {
                return true;
            }
        }
        return false;
    }

    private static void configureLogger(Logger log) {
        Handler discordLogHandler = new LogToDiscordChannelHandler(api, Settings.getInstance().getLogChannelID());
        discordLogHandler.setLevel(Level.INFO);
        discordLogHandler.setFormatter(new LogFormatter());
        log.addHandler(discordLogHandler);


//        //Create a secure socket
//        try {
//            SSLSocket socket = SecurityUtils.get().createSocket(Settings.LOG_SERVER_IP, Settings.LOG_SERVER_PORT);
//            socket.setSoTimeout(10000);
//            try {
//                Handler streamHandler = new LogStreamHandler(socket.getOutputStream(), new LogFormatter());
//                streamHandler.setLevel(Level.INFO);
//                log.addHandler(streamHandler);
//            } catch (IOException e) {
//                log.log(Level.WARNING, () -> String.format("Error on adding Log Socket Handler: %s", e.getMessage()));
//                e.printStackTrace();
//            }
//        } catch(IOException e) {
////            e.printStackTrace();
//            log.log(Level.WARNING, () -> String.format("Log server not reachable! IP: %s PORT: %d", Settings.LOG_SERVER_IP, Settings.LOG_SERVER_PORT));
//            return;
//        }
    }

    private static String getSlashArgumentsString(SlashCommandEvent event) {
        try {

            var options = event.getOptions();
            List<String> optionStrings = new ArrayList<>();
            for (var option : options) {
                var type = option.getType();
                String name = option.getName();
                String value;

                switch (type) {
                    case USER:
                        value = event.getGuild().getMemberById(option.getAsLong()).getUser().getAsTag();
                        break;
                    case CHANNEL:
                        value = event.getGuild().getGuildChannelById(option.getAsLong()).getName();
                        break;
                    case ROLE:
                        value = event.getGuild().getRoleById(option.getAsLong()).getName();
                        break;
                    case STRING:
                        value = option.getAsString();
                        break;
                    default:
                        value = String.valueOf(option.getAsLong());
                }
                optionStrings.add(String.format("[%s] (%s=%s)", type.name(), name, value));
            }
            return "[" + String.join(", ", optionStrings) + "]";
        } catch (NullPointerException e) {
            return event.getOptions().toString();
        }
    }
}
