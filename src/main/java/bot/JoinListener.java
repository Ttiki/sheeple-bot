package bot;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import javax.annotation.Nonnull;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JoinListener extends ListenerAdapter {
    private Timer kickTimer; //kick new members after 15 minutes if they haven't registered

    public JoinListener() {
        this.kickTimer = new Timer(true);
    }


    @Override
    public void onGuildMemberJoin(@Nonnull GuildMemberJoinEvent event) {
        if(!event.getGuild().getId().equals(Settings.getInstance().getGuildID())) {
            return;
        }
        long gracePeriod = Settings.getInstance().getKickUnregisteredMembersAfter_ms();

        Logger logger = Logger.getLogger(Settings.LOGGER_NAME);
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                Member newMember = event.getMember();
                Guild guild = event.getJDA().getGuildById(Settings.getInstance().getGuildID());
                if (guild == null) {
                    return;
                }
                Role memberRole = guild.getRoleById(Settings.getInstance().getMemberRoleID());
                if (memberRole == null) {
                    logger.log(Level.SEVERE, () -> String.format("Member role not found. ID: %s. Auto-Kick disabled", Settings.getInstance().getMemberRoleID()));
                    event.getJDA().removeEventListener(this);
                    return;
                }

                String kickReason = Settings.getInstance().getKickReason();

                if (guild.getMember(newMember.getUser()) != null && !newMember.getRoles().contains(memberRole)) {
                    try {
                        guild.kick(newMember, kickReason).queue();
                        logger.log(Level.INFO, () -> String.format("Kicked %s: %s", newMember.getUser().getAsTag(), kickReason));
                    }
                    catch (Exception e) {
                        logger.log(Level.SEVERE, e, () -> String.format("Exception on trying to kick member %s (ID: %d): ", newMember.getUser().getAsTag(), newMember.getIdLong()));
                    }
                }
            }
        };
        kickTimer.schedule(task, gracePeriod);
    }
}
