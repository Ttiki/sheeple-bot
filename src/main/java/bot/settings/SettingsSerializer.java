package bot.settings;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class SettingsSerializer extends JsonSerializer<Settings> {

    @Override
    public void serialize(Settings value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("startDate", value.getStartDate().toString());
        gen.writeStringField("piHighscore", String.valueOf(value.getPiHighscore()));
        gen.writeStringField("resetCounter", String.valueOf(value.getResetCounter()));
        gen.writeStringField("discordToken", value.getDiscordToken());
        gen.writeStringField("guildID", value.getGuildID());
        gen.writeStringField("roleChannelID", value.getRoleChannelID());
        gen.writeStringField("registrationChannelID", value.getRegistrationChannelID());
        gen.writeStringField("mutedChannelID", value.getMutedChannelID());
        gen.writeStringField("victimReportChannelID", value.getVictimReportChannelID());
        gen.writeStringField("logChannelID", value.getLogChannelID());
        gen.writeStringField("memberRoleID", value.getMemberRoleID());
        gen.writeStringField("mutedRoleID", value.getMutedRoleID());
        gen.writeStringField("kickReason", value.getKickReason());
        gen.writeStringField("registrationGracePeriod", String.valueOf(value.getKickUnregisteredMembersAfter_ms()));
        gen.writeStringField("blackListURL", value.getBlackListURL());

        gen.writeArrayFieldStart("sheepitAdminRoleIDs");
        value.getSuperRoleIDs().forEach(s -> {
            try {
                gen.writeString(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        gen.writeEndArray();

        gen.writeArrayFieldStart("assignableRoleIDs");
        value.getAssignableRoles().forEach(s -> {
            try {
                gen.writeString(s);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        gen.writeEndArray();

        gen.writeStringField("statsEndpoint", value.getStatsEndpoint());

        gen.writeFieldName("messageConfig");
        gen.writeStartObject();
        gen.writeStringField("messageTitle", value.getMessageTitle());
        gen.writeStringField("messageAuthor", value.getMessageAuthor());
        gen.writeStringField("messageURL", value.getMessageURL());
        gen.writeStringField("messageIconURL", value.getMessageIconURL());
        gen.writeStringField("messageThumbnailURL", value.getMessageThumbnailURL());
        gen.writeStringField("messageColor", value.getMessageColor());
        gen.writeEndObject();

        gen.writeArrayFieldStart("commands");
        value.getCommands().forEach((s, s2) -> {
            try {
                gen.writeObject(s2);
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        gen.writeEndArray();
        gen.writeEndObject();

    }
}
