package bot.settings;

import bot.SheepleBot;
import bot.commands.Command;
import bot.commands.DefaultCommands;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.MessageEmbed;
import org.jetbrains.annotations.NotNull;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The Settings class that stores a singleton instance with all the global settings of the bot.
 */
@Getter
@Setter
@JsonSerialize(using = SettingsSerializer.class)
@JsonDeserialize(using = SettingsDeserializer.class)
public class Settings {

    /**
     * Path to the config file that is to be loaded at the start of the bot
     */
    @Getter @Setter private static String configFile = "sheeple.json";

    /**
     * A Map with the default commands the bot offers. These will be added if not specified in the config file.
     * In case the config file specifies any changes over the default values (except the name) the config file takes priority
     */
    public static final Map<String, Command> DEFAULT_COMMANDS = setupDefaultCommands();

    /**
     * Name to reference the application wide logger (configured in {@link SheepleBot})
     */
    public static final String LOGGER_NAME = "SHEEPLE_LOGGER";

    /**
     * How long members that have been marked as twits with the {@link DefaultCommands#TWIT} command are going to be ignored
     */
    public static final Duration TWIT_IGNORE_TIME_MINUTES = Duration.ofMinutes(30);

    /**
     * Version of the bot as defined in build.gradle
     */
    public static final String BOT_VERSION = getVersion();


    //the singleton
    private static Settings instance;

    /**
     * The following messages all relate to the appearance of the embedded messages the bot uses for its responses.
     * https://raw.githubusercontent.com/DV8FromTheWorld/JDA/assets/assets/docs/embeds/07-addField.png
     */
    private String messageTitle;
    private String messageAuthor;
    private String messageURL;
    private String messageIconURL;
    private String messageThumbnailURL;
    private String messageColor;

    /**
     * simple thread synchronization object
     */
    private Object globalLock;

    /**
     * The last time the {@link DefaultCommands#RESET} command has been called
     */
    private LocalDate startDate;

    /**
     * stores the longest time period between two resets
     */
    private long piHighscore;

    /**
     * counts how often !sheeple-reset has been called
     */
    private long resetCounter;

    /**
     * map with all the commands the bot offers
     */
    private HashMap<String, Command> commands;

    /**
     * The token for the Discord API
     */
    private String discordToken;

    /**
     * used to create the embedded messages the bot uses for responses
     */
    EmbedBuilder messageBuilder;

    /**
     * the channel id of the role-request channel
     */
    private String roleChannelID;

    /**
     * the member registration channel id
     */
    private String registrationChannelID;

    /**
     * the i-am-muted channel id --> only visible channel for muted members
     */
    private String mutedChannelID;

    /**
     * the i-was-muted channel id --> where to send info about arrested members
     */
    private String victimReportChannelID;

    /**
     * the ID for the "Muted" role, needed for the arrest command
     * @see bot.commands.ArrestHandler
     */
    private String mutedRoleID;

    /**
     * the ID of the member role that is to be assigned in the { @link #registrationChannelID }
     */
    private String memberRoleID;

    /**
     * The API endpoint for sheepit statistics
     */
    private String statsEndpoint;
    /**
     * a list of all the roles that can be requested by the bot via {@link DefaultCommands#JOIN}, {@link DefaultCommands#LEAVE}
     */
    private ArrayList<String> assignableRoles;

    /**
     * Amount of time before messages get deleted. Used for messages in the role-request channel and some error messages
     */
    private final int deleteDelaySeconds;

    /**
     * Discord members with any of these role IDs can execute any command regardless of their permissions
     */
    private LinkedList<String> superRoleIDs;

    /**
     * a map with currently ignored members (twits)
     */
    private HashMap<String, LocalTime> ignoredMembers;

    /**
     * The id of the channel that will receive the log
     */
    private String logChannelID;

    /**
     * The id of the discord server the bot is on. Its mostly for debugging purposes, because guild-registered slash commands propagate faster
     */
    private String guildID;

    /**
     * The time after which new server members get kicked if they don't have the members role
     */
    private long kickUnregisteredMembersAfter_ms;

    /**
     * The reason message for kicked unregistered members
     */
    private String kickReason;

    /**
     * URL to txt containing blacklisted websites
     */
    private String blackListURL;

    private static HashMap<String, Command> setupDefaultCommands() {

        HashMap<String, Command> map = new HashMap<>();
        for(DefaultCommands cmd : DefaultCommands.values()) {
            map.put(cmd.getCmd().getName(), cmd.getCmd());
        }

        return map;
    }

    public static Settings getInstance() {
        if(instance == null) {
            instance = create(configFile);
        }
        return instance;
    }

    /**
     * Don't use the constructor to create instances. It just exists for the {@link SettingsDeserializer}.
     * Use {@link #create(String field)} instead
     */
    Settings() {
        globalLock = new Object();
        roleChannelID = "836356580978458656";
        registrationChannelID = "868181076940566568";
        mutedChannelID = "878190010547986462";
        memberRoleID = "867966212670369793";
        mutedRoleID = "541877836105908233";
        logChannelID = "879821224505647135";
        victimReportChannelID = "822923974656393276";
        commands = new HashMap<>();
        startDate = LocalDate.now();
        ignoredMembers = new HashMap<>();
        statsEndpoint = "";
        blackListURL = "https://dl-eu.opendataapi.net/discordspam/list.txt";

        messageTitle = "SheepleBot";
        messageAuthor = "Sheep it Render Farm";
        messageURL = "https://sheepit-renderfarm.com";
        messageIconURL = "https://sheepit-renderfarm.com/media/image/title.png";
//        messageThumbnailURL = "https://sheepit-renderfarm.com/media/image/title.png";
        messageThumbnailURL = "https://cdn.discordapp.com/attachments/801190814050156584/838405716179025950/sheepit-logo_sheep-white_big_grey.png";
        messageColor = "#e06d58";

        messageBuilder = new EmbedBuilder();
        messageBuilder.setTitle(messageTitle);
        messageBuilder.setAuthor(messageAuthor, messageURL, messageIconURL);
        messageBuilder.setColor(Color.decode(messageColor));
        messageBuilder.setThumbnail(messageThumbnailURL);

        superRoleIDs = new LinkedList<>();
        Arrays.stream(DefaultSuperRoles.values()).iterator().forEachRemaining(r -> superRoleIDs.add(r.getId()));

        assignableRoles = new ArrayList<>();
        Arrays.stream(DefaultAssignableRoles.values()).iterator().forEachRemaining(r->assignableRoles.add(r.getId()));

        deleteDelaySeconds = 15;

        guildID = "253355867938750485";

        kickUnregisteredMembersAfter_ms = 15 * 60 * 1000;

        kickReason = "Failed to register in time";
    }

    /**
     * Create bot.settings.Settings instances through this method.
     * Dont use the constructor. Its just public for the {@link SettingsDeserializer}
     * @param file path to config file
     * @return
     */
    private static Settings create(String file) {
        if(file == null) {
            file = configFile;
        }
        Settings result = loadSettingsFromConfigFile(file);

        for(Map.Entry<String, Command> entry : DEFAULT_COMMANDS.entrySet()) {
            result.commands.putIfAbsent(entry.getKey(), entry.getValue());
        }

        try {
            result.save(file);
        } catch (IOException e) {
            Logger.getLogger(LOGGER_NAME).log(Level.SEVERE,
                    String.format("Exception while trying to save the settings file: %s", file), e);
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Save current config to the default config file
     * @return true if successful, else false
     * @throws IOException
     */
    public boolean save() throws IOException {
        return save(configFile);
    }

    /**
     * Save instance as json file at {@link Settings#configFile}
     * @return true if successful, else false
     * @throws IOException
     */
    public boolean save(String file) throws IOException {
        boolean success = false;
        String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this);
        Files.write(Paths.get(file), json.getBytes());
        success = true;
        return success;
    }

    private static Settings loadSettingsFromConfigFile(String file) {
        Settings result = new Settings();
        File fileObject = new File(file);
        if (fileObject.exists() && !fileObject.isDirectory()) {
            try {
                String json = new String((Files.readAllBytes(Paths.get(file))));
                result = new ObjectMapper().readValue(json, Settings.class);
                if(result == null) {
                    return null;
                }
            } catch (IOException e) {
                Logger.getLogger(LOGGER_NAME).log(Level.SEVERE,
                        String.format("Exception while trying to load settings file %s", file, e));
                e.printStackTrace();
                return null;
            }
        }
        return result;
    }

    public MessageEmbed createMessage(@NotNull String msg) {
        return messageBuilder.setDescription(Objects.requireNonNull(msg)).build();
    }

    public MessageEmbed createMessage(@NotNull String description, @NotNull MessageEmbed.Field... fields) {
        messageBuilder.setDescription(Objects.requireNonNull(description));
        Arrays.stream(fields).forEach(messageBuilder::addField);
        MessageEmbed msg = messageBuilder.build();
        messageBuilder.getFields().clear();
        return msg;
    }

    public MessageEmbed createImageMessage(String description, String imageURL) {
        messageBuilder.setImage(imageURL);
        messageBuilder.setDescription(description);
        MessageEmbed result = messageBuilder.build();
        messageBuilder.setImage(null);
        return result;
    }

    public MessageEmbed createMessage(@NotNull String description, String footerText, @NotNull MessageEmbed.Field... fields) {
        messageBuilder.setFooter(footerText);
        MessageEmbed result = createMessage(description, fields);
        messageBuilder.setFooter("");
        return result;
    }

    public MessageEmbed createMessage(@NotNull String description, String title, String thumbnailURL, String footerText, @NotNull MessageEmbed.Field... fields) {
        messageBuilder.setThumbnail(thumbnailURL);
        messageBuilder.setTitle(title);
        MessageEmbed result = createMessage(description, footerText, fields);
        messageBuilder.setThumbnail(null);
        messageBuilder.setTitle(messageTitle);
        return result;
    }

    private static String getVersion() {
        try {
            byte[] data = Settings.class.getClassLoader().getResourceAsStream("version.txt").readAllBytes();
            String version = new String(data).trim();
            return version;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
