package bot;

import bot.settings.Settings;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpamDetection extends ListenerAdapter {

    private static final Pattern URL_PATTERN = Pattern.compile("[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b([-a-zA-Z0-9()@:%_\\+.~#?&//=]*)");
    private static final long REPING_AFTER = 5 * 60 * 1000;
    private static final String URL_FILE = "scam_urls.txt";

    private static List<String> WHITELIST_URLS = List.of("https://discordapp.com", "https://discordapp.net", "https://discord.com",
        "https://discord.new", "https://discord.gift", "https://discord.gifts", "https://discord.media", "https://discord.gg",
        "https://discord.co", "https://discord.app", "https://dis.gd");

    private List blackListURLs = null;

    private Pattern banWordsPattern = Pattern.compile("((N|n)(i|I)(t|T)(r|R)(o|O))|(C|c)(S|s):?(G|g)(O|o)");
    private List alertPersons = List.of(631255370656120843L, 256158358430547968L);
    private Object blackListLock;
    private TimerTask backgroundTask;
    private final List<Long> detectedSpammers;
    private Timer pingTimer; //delete spammer id out of detectedSpammers list after set time so their messages will be scanned again


    public SpamDetection() {
        detectedSpammers = new ArrayList<>();
        pingTimer = new Timer("Ping Timer");
        blackListLock = new Object();
         backgroundTask = new TimerTask() {
             @Override
             public void run() {
                 if (blackListURLs == null)
                     updateBlacklist();
                 else {
                     synchronized (blackListLock) {
                        updateBlacklist();
                     }
                 }
             }
         };
        backgroundTask.run();
        BackgroundTaskHandler.get().addBackgroundTask(backgroundTask, 60 * 60 * 1000, 60 * 60 * 1000);
    }

    public void onMessageReceived(MessageReceivedEvent event) {

        if (event.getAuthor().isBot() || event.getChannel().getId().equals(Settings.getInstance().getLogChannelID())
        || event.getMember().hasPermission(Permission.ADMINISTRATOR) || event.getMember().hasPermission(Permission.MANAGE_CHANNEL)) {
            return;
        }

        boolean botHasPinged = false;
        synchronized (detectedSpammers) {
            botHasPinged = detectedSpammers.contains(event.getAuthor().getIdLong());
        }

        if (botHasPinged) {
            return; //dont ping mods for each spam message
        }

        String msg = event.getMessage().getContentRaw();
        List<String> urls = new ArrayList<>();

        Matcher matcher = URL_PATTERN.matcher(msg);
        while (matcher.find()) {
            int pos = matcher.start();
            String url = msg.substring(pos, matcher.end());
            if (url.endsWith("/")) {
                url = url.substring(0, url.length() - 1);
            }
            urls.add(url);
        }

        if (!urls.isEmpty()) {
            boolean isBlacklisted = false;

            if (hasBlacklist()) {
                synchronized (blackListLock) {
                    isBlacklisted = !Collections.disjoint(blackListURLs, urls);
                }
            }

            matcher = banWordsPattern.matcher(msg);
            boolean allWhitelisted = true;
            if(matcher.find() ||
                    (event.getMessage().getMentions(Message.MentionType.USER).isEmpty() && (msg.contains("@everyone") ||
                            msg.contains("@here")))) {
                boolean tmpURLisWhitelisted = false;
                if (!isBlacklisted) {
                    for (String url : urls) {
                        for (String whiteListURL : WHITELIST_URLS) {
                            if (whiteListURL.contains(url)) {
                                tmpURLisWhitelisted = true;
                                break;
                            }
                        }
                        if (!tmpURLisWhitelisted) {
                            allWhitelisted = false;
                            break;
                        }
                    }
                }
            }

            if (isBlacklisted || !allWhitelisted) {
                printURLsToFile(URL_FILE, urls);

                detectedSpammers.add(event.getAuthor().getIdLong());
                TimerTask deleteIDTask = new TimerTask() {
                    @Override
                    public void run() {
                        synchronized (detectedSpammers) {
                            detectedSpammers.remove(event.getAuthor().getIdLong());
                        }
                    }
                };
                pingTimer.schedule(deleteIDTask, REPING_AFTER);

                StringBuilder builder = new StringBuilder();
                alertPersons.forEach(id -> builder.append("<@").append(id).append("> "));
                Logger.getLogger(Settings.LOGGER_NAME).log(
                        Level.INFO,
                        () -> String.format("%s Potential Spam user: %s https://discord.com/channels/%d/%d/%d",
                                builder.toString(), event.getMember().getAsMention(),
                                event.getGuild().getIdLong(), event.getTextChannel().getIdLong(), event.getMessageIdLong()));
            }
        }
    }

    private void printURLsToFile(String urlFile, List<String> urls) {

        try (FileOutputStream fout = new FileOutputStream(urlFile, true)){
            urls.forEach(
                    url -> {
                        try {
                            fout.write((url + "\n").getBytes(StandardCharsets.UTF_8));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
            );
        }
        catch (IOException e) {

        }
    }

    public void updateBlacklist() {
        String updateURL = Settings.getInstance().getBlackListURL();

        try {
            URL url = new URL(updateURL);
            InputStream is = url.openStream();
            byte[] buffer = is.readAllBytes();
            is.close();
            String links = new String(buffer);
            blackListURLs = List.of(links.split("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean hasBlacklist() {
        return blackListURLs != null && !blackListURLs.isEmpty();
    }
}
