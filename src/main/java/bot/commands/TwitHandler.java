package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.ErrorResponseException;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.time.LocalTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Puts guild members in a timeout. The bot will ignore them for the amount of time specified.
 */
public class TwitHandler extends BaseCommandHandler {
    public static final String twitOption = "twit";

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {
         User twit = event.getOption(twitOption).getAsUser();

         if(twit == null) {
             return Optional.of(Settings.getInstance().createMessage("You forgot to mention (a) twit(s)"));
         }
        try {
            LocalTime time = LocalTime.now();
            String twitID = twit.getId();
            Settings.getInstance().getIgnoredMembers().remove(twitID);
            Settings.getInstance().getIgnoredMembers().put(twitID, time);

        } catch (InsufficientPermissionException | HierarchyException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, "Exception: ", e);
            return Optional.of(Settings.getInstance().createMessage(e.getMessage()));
        } catch (ErrorResponseException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, "Exception: ", e);
            return Optional.empty();
        }

        String output = String.format(command.getOutput(), twit.getAsTag(), Settings.TWIT_IGNORE_TIME_MINUTES.toMinutes());
        MessageEmbed msg = Settings.getInstance().createMessage(output);
        return Optional.of(msg);
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        Member caller = event.getMember();
        Member twit = event.getOption(twitOption).getAsMember();
        List<Role> sheepItRoles = Settings.getInstance().getSuperRoleIDs().stream()
                .map(r -> event.getGuild().getRoleById(r)).collect(Collectors.toList());

        boolean callerHasSheepItRole = !Collections.disjoint(caller.getRoles(), sheepItRoles);
        boolean twitIsInvalid = false;
        if(twit == null) {  //twit is not a member of this guild
            twitIsInvalid = false;
        }
        else {
            twitIsInvalid = !Collections.disjoint(twit.getRoles(), sheepItRoles) || twit.hasPermission(Permission.MANAGE_ROLES);
        }

        if(twitIsInvalid) {
            return RequirementsCheckResult.INVALID_ARGUMENT;
        }
        if(super.meetsRequirements(command, event).equals(RequirementsCheckResult.OK) || callerHasSheepItRole) {
            return RequirementsCheckResult.OK;
        }
        else {
            return RequirementsCheckResult.MUST_BE_SHEEPMIN;
        }
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.USER, twitOption, "The twit that shall be put in timeout", true)
                .queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.USER, twitOption, "The twit that shall be put in timeout", true)
                .queue();
    }
}
