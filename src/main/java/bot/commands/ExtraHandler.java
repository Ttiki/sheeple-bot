package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.util.Optional;

/**
 * Handles any commands that were added through the config file and dont require any special actions
 */
public class ExtraHandler extends BaseCommandHandler{
    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {

        return Optional.of(Settings.getInstance().createMessage(command.getOutput()));
    }
}
