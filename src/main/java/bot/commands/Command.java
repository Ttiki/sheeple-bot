package bot.commands;

import bot.settings.Settings;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.Objects;
import java.util.Optional;


/**
 * A tupel for chat commands
*/
@Getter
@JsonDeserialize(using = CommandDeserializer.class)
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)  //creates default constructor set  to private and initializes all fields with default values
public class Command {
    private final String name;
    private final String output;
    private final Permission requiredPermission;
    private final String description;
    private final CommandCategories category;
    @JsonIgnore private final boolean ephemeral;
    @JsonIgnore @Getter(AccessLevel.PACKAGE)    //see this issue https://github.com/FasterXML/jackson-databind/issues/1226
    private final boolean isDefaultCommand;

    @JsonIgnore @Getter(AccessLevel.NONE)
    private final CommandHandler handler;
//    private CommandData cmdData;

    /**
     * @param name The chat command, will most likely be always String
     * @param output The corresponding output
     * @param description a short description of the command. will be displayed by !sheeple-help
     * @param requiredPermission The required permission to execute this command
     *      *  *           For all available permissions, see {@link net.dv8tion.jda.api.Permission}.
     *      *  *           Some notable ones are <code>MANAGE_CHANNEL, MESSAGE_READ</code>
     * @param handler
     */
    public Command(String name, String output, @Nullable String description, @NotNull CommandCategories category,
                   @NotNull Permission requiredPermission, @NotNull CommandHandler handler, boolean isDefaultCommand, boolean isEphemeral) {
        this.name = Objects.requireNonNull(name);
        this.output = Objects.requireNonNull(output);
        this.requiredPermission = requiredPermission;
        this.handler = handler;
        this.description = Objects.isNull(description) ? "" : description;
        this.isDefaultCommand = isDefaultCommand;
        this.category = category;
        this.ephemeral = isEphemeral;
    }

    public Command(@NotNull String name, @NotNull String output, @Nullable String description, @NotNull CommandCategories category,
                   @NotNull Permission requiredPermission) {
        this.name = Objects.requireNonNull(name);
        this.output = Objects.requireNonNull(output);
        this.description = Objects.isNull(description) ? "" : description;
        this.requiredPermission = Objects.requireNonNull(requiredPermission);
        this.category = category;
        this.ephemeral = false;

        if(Settings.DEFAULT_COMMANDS.containsKey(name)) {
            handler = Settings.DEFAULT_COMMANDS.get(name).handler;
            this.isDefaultCommand = true;
        }
        else {
            handler = new ExtraHandler();
            this.isDefaultCommand = false;
        }
    }

    public Optional<MessageEmbed> handle(SlashCommandEvent event) { return handler.handle(this, event); }

    public RequirementsCheckResult meetsRequirements(SlashCommandEvent event) { return handler.meetsRequirements(this, event); }

    public void registerCommand(JDA api) {
        handler.registerCommand(this, api);
    }

    public void registerCommand(Guild guild) { handler.registerCommand(this, guild);}

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("**").append(name).append("**");
        if(!description.isEmpty()) {
            builder.append(" - ").append(description);
        }
        return builder.toString();
    }
}
