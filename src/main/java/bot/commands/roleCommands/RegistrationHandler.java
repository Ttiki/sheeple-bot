package bot.commands.roleCommands;

import bot.commands.BaseCommandHandler;
import bot.commands.Command;
import bot.commands.RequirementsCheckResult;
import bot.settings.Settings;
import lombok.AllArgsConstructor;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RegistrationHandler extends BaseCommandHandler {

    private ExecutorService executor;

    public RegistrationHandler() {
        this.executor = Executors.newCachedThreadPool();
    }

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {

        event.deferReply().queue(m -> m.deleteOriginal().queue());
        executor.submit(new RegistrationHandlerRunnable(event));
        return Optional.empty();
    }



    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        if(event.getMember().getRoles().contains(event.getGuild().getRoleById(Settings.getInstance().getMemberRoleID()))) {
            event.replyEmbeds(Settings.getInstance().createMessage("You are already registered")).setEphemeral(true).queue();
            return RequirementsCheckResult.IGNORE;
        }
        return RequirementsCheckResult.OK;
    }



    @AllArgsConstructor
    private class RegistrationHandlerRunnable implements Runnable {

        protected SlashCommandEvent event;

        @Override
        public void run() {
            Member member = event.getMember();
            Role memberRole = event.getGuild().getRoleById(Settings.getInstance().getMemberRoleID());
            register(member, memberRole);
        }

        private void register(Member member, Role role) {
            try {
                if(role == null) {
                    event.getHook().sendMessageEmbeds(Settings.getInstance().createMessage("Couldn't find the member role")).queue();
                    return;
                }
                if(member == null) {
                    event.getHook().sendMessageEmbeds(Settings.getInstance().createMessage("Couldn't find the member")).queue();
                }

                event.getGuild().addRoleToMember(member, role).queue();
            } catch (Exception e) {
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, e, () -> "Exception on trying to register " + member);
            }
        }
    }
}
