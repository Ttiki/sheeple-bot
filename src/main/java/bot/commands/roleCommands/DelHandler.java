package bot.commands.roleCommands;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.internal.entities.RoleImpl;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DelHandler extends AdminRoleCommandHandler{
    @Override
    protected Optional<MessageEmbed> manage(Command command, SlashCommandEvent event) {

        Role role = new RoleImpl(0, event.getGuild());
        try {
            role = event.getOption("role").getAsRole();
        } catch(NullPointerException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, "Exception: role option is null", e);
            return Optional.of(Settings.getInstance().createMessage("Could not find option: role"));
        }


        //check that role is in fact managed by the bot
        if(!Settings.getInstance().getAssignableRoles().contains(role.getId()))
            return Optional.of(Settings.getInstance().createMessage("Role is not requestable to begin with. Check your input: `!remove <role id or ping>`"));

        try {
            //remove role from list
            Settings.getInstance().getAssignableRoles().remove(role.getId());
            Settings.getInstance().save();
        } catch (IOException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, String.format("Exception on trying to remove role %s", role), e);
            return Optional.of(Settings.getInstance().createMessage(e.getMessage()));
        }
        return Optional.of(Settings.getInstance().createMessage(String.format(command.getOutput(), "<@&" + role.getId() + ">")));
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.ROLE, "role", "The role you want to remove from the requestable roles", true)
                .queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.ROLE, "role", "The role you want to remove from the requestable roles", true)
                .queue();
    }
}
