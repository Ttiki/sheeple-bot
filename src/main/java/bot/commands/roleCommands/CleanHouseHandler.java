package bot.commands.roleCommands;

import bot.commands.Command;
import bot.commands.RequirementsCheckResult;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.GenericInteractionCreateEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.MissingAccessException;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.RejectedExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Command to wipe all messages in the role-request channel. It will keep the first message in the channel
 */
public class CleanHouseHandler extends BaseRoleManagementCommandHandler {
    private Thread thread;

    @Override
    protected Optional<MessageEmbed> execute(Command command, SlashCommandEvent event) {
        TextChannel channel = event.getGuild().getTextChannelById(Settings.getInstance().getRoleChannelID());
        if(channel == null) {
            return Optional.of(Settings.getInstance().createMessage(String.format("No channel with ID %s found!",
                    Settings.getInstance().getRoleChannelID())));
        }
        thread = new Thread(() -> wipeAllButFirst(channel, event));
        thread.start();
        event.reply("Deleting").setEphemeral(false).queue(m -> m.deleteOriginal().queue());
        return Optional.empty();
    }

    protected void wipeAllButFirst(TextChannel channel, GenericInteractionCreateEvent event) {
        try {
            synchronized (Settings.getInstance().getGlobalLock()) {
                MessageHistory history = channel.getHistoryFromBeginning(1).complete(); //retrieve first message so deletion starts with the second message
                List<Message> messages = history.retrieveFuture(20).complete();
                while (!messages.isEmpty()) {
                    if (messages.size() < 2) {   //deleteMessages requires between 2-100 messages
                        messages.get(0).delete().queue(
                                null, error -> { }
                        );
                        break;
                    }
                    channel.purgeMessages(messages);
                    messages = history.retrieveFuture(20).complete();
                }
            }
        } catch(RejectedExecutionException | NullPointerException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception: ");
        } catch(MissingAccessException | IllegalArgumentException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception: ");
            event.getHook().sendMessageEmbeds(Settings.getInstance().createMessage(e.getMessage())).queue();
            e.printStackTrace();
        }
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        Member caller = event.getMember();
        List<Role> sheepItRoles = Settings.getInstance().getSuperRoleIDs().stream()
                .map(r -> event.getGuild().getRoleById(r)).collect(Collectors.toList());

        boolean callerHasSheepItRole = !Collections.disjoint(caller.getRoles(), sheepItRoles);

        if(super.meetsRequirements(command, event).equals(RequirementsCheckResult.OK) || callerHasSheepItRole) {
            return RequirementsCheckResult.OK;
        }
        else {
            return RequirementsCheckResult.MUST_BE_SHEEPMIN;
        }
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        try {
            String desc = String.format(command.getDescription(),
                    guild.getRoleById(Settings.getInstance().getRoleChannelID()).getName());
            guild.upsertCommand(command.getName(), desc).queue();
        } catch (NullPointerException e) {
            guild.upsertCommand(command.getName(), String.format(command.getDescription(), "role-request")).queue();
        }
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        String desc = String.format(command.getDescription(),
                api.getRoleById(Settings.getInstance().getRoleChannelID()).getName());
        api.upsertCommand(command.getName(), desc).queue();
    }
}
