package bot.commands.roleCommands;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddHandler extends AdminRoleCommandHandler{

    private static final String option = "role";
    @Override
    protected Optional<MessageEmbed> manage(Command command, SlashCommandEvent event) {

        Role role = event.getOption(option).getAsRole();
        try {
            if(role.isPublicRole())
                return Optional.of(Settings.getInstance().createMessage("Can't add this role"));

            //check that id is not already on the list of managed roles
            if(Settings.getInstance().getAssignableRoles().contains(role.getId()))
                return Optional.of(Settings.getInstance().createMessage(String.format("Role is already requestable:  %s",
                        role.getName())));

            //add role and write settings to file
            Settings.getInstance().getAssignableRoles().add(role.getId());
            Settings.getInstance().save();
        } catch(NumberFormatException | IOException e) {
            e.printStackTrace();
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> String.format("Role: %s %s", role.getAsMention(), role.getName()));
            return Optional.of(Settings.getInstance().createMessage(e.getMessage()));
        }

        return Optional.of(Settings.getInstance().createMessage(String.format(command.getOutput(),
                "<@&" + role.getId() + ">")));
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.ROLE, option, "The role you want to add to the requestable roles", true)
                .queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.ROLE, option, "The role you want to add to the requestable roles", true)
                .queue();
    }
}
