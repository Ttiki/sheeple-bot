package bot.commands.roleCommands;

import bot.MessageDeleteErrorHandler;
import bot.commands.Command;
import bot.commands.RequirementsCheckResult;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class RoleRequestCommandHandler extends BaseRoleManagementCommandHandler{

    protected abstract Optional<MessageEmbed> changeRole(Command command, SlashCommandEvent event, Role role);

    protected Optional<MessageEmbed> execute(Command command, SlashCommandEvent event) {

        //get the sender of the command
        Member member = event.getMember();
        if(member == null) {
            Logger.getLogger(getClass().getName()).log(Level.WARNING, "event.getMember() returned null");
            return Optional.of(Settings.getInstance().createMessage("event.getMember() returned null."));
        }


        try {
            //get requested role argument
            Role role = event.getOption("role").getAsRole();

            return changeRole(command, event, role);

        } catch(IllegalArgumentException | InsufficientPermissionException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.INFO, String.format("%s%nMember: %s", e.getMessage(), member.getEffectiveName()));
            return Optional.of(Settings.getInstance().createMessage(String.format("%s%n", e.getMessage())));
        }
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {

        var option = event.getOption("role");
        if(option == null || event.getMember() == null) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING,
                    () -> String.format("Received null objects. option: %s\n" +
                            "Invocating member: %s", option, event.getMember()));
            return RequirementsCheckResult.ERROR;
        }

        RequirementsCheckResult baseCheck = super.meetsRequirements(command, event);
        if(baseCheck != RequirementsCheckResult.OK)
            return baseCheck;

        Role requestedRole = option.getAsRole();

        //check if role is allowed to be assigned/removed
        if(!Settings.getInstance().getAssignableRoles().contains(requestedRole.getId())) {
            MessageEmbed response = Settings.getInstance().createMessage("Role not manageable. Run `/role-list` to get a list of available roles");
            event.replyEmbeds(response).setEphemeral(command.isEphemeral()).queue(
                    msg -> msg.deleteOriginal().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                            null, new MessageDeleteErrorHandler())
            );
            return RequirementsCheckResult.IGNORE;  //should be INVALID_ARGUMENT but we want this custom error message instead of the default one
        }
        return baseCheck;
    }
}
