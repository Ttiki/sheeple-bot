package bot.commands.roleCommands;

import bot.commands.Command;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.util.Optional;

public class ListRolesHandler extends BaseRoleManagementCommandHandler{

    @Override
    protected Optional<MessageEmbed> execute(Command command, SlashCommandEvent event) {

        StringBuilder builder = new StringBuilder();
        for(String id : Settings.getInstance().getAssignableRoles()) {
            builder.append("<@&").append(id).append(">\n");
        }
        String output = String.format(command.getOutput(), builder);
        return Optional.of(Settings.getInstance().createMessage(output));
    }
}
