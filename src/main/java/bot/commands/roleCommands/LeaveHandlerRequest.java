package bot.commands.roleCommands;

import bot.MessageDeleteErrorHandler;
import bot.commands.Command;
import bot.commands.RequirementsCheckResult;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class LeaveHandlerRequest extends RoleRequestCommandHandler {

    @Override
    protected Optional<MessageEmbed> changeRole(Command command, SlashCommandEvent event, Role role) {
        Member member = event.getMember();

        Objects.requireNonNull(event.getGuild()).removeRoleFromMember(member, role).queue();

        String response = String.format(command.getOutput(), member.getEffectiveName(), String.format("<@&%s>", role.getId()));

        return Optional.of(Settings.getInstance().createMessage(response));
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        try {
            String channelName = api.getTextChannelById(Settings.getInstance().getRoleChannelID()).getName();
            api.upsertCommand(command.getName(), String.format(command.getDescription(), channelName))
                    .addOption(OptionType.ROLE, "role", "The role you want to leave", true)
                    .queue();
        } catch (NullPointerException e) {
            api.upsertCommand(command.getName(), String.format(command.getDescription(), "role-request"))
                    .addOption(OptionType.ROLE, "role", "The role you want to leave", true)
                    .queue();
        }
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        try {
            String channelName = guild.getTextChannelById(Settings.getInstance().getRoleChannelID()).getName();
        guild.upsertCommand(command.getName(), String.format(command.getDescription(), channelName))
                .addOption(OptionType.ROLE, "role", "The role you want to leave", true)
                .queue();
        } catch (NullPointerException e) {
        guild.upsertCommand(command.getName(), String.format(command.getDescription(), "role-request"))
                .addOption(OptionType.ROLE, "role", "The role you want to leave", true)
                .queue();
        }
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {

        RequirementsCheckResult baseCheck = super.meetsRequirements(command, event);
        if(baseCheck != RequirementsCheckResult.OK)
            return baseCheck;

        var option = event.getOption("role");
        Role requestedRole = option.getAsRole();

        if(!event.getMember().getRoles().contains(requestedRole)) {
            event.replyEmbeds(Settings.getInstance().createMessage("You dont have this role")).queue(
                    m -> m.deleteOriginal().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                    null, new MessageDeleteErrorHandler())
            );
            return RequirementsCheckResult.IGNORE;  //should be INVALID_ARGUMENT but we want this custom error message instead
        }

        return baseCheck;
    }
}
