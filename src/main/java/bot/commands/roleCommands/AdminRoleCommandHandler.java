package bot.commands.roleCommands;

import bot.commands.Command;
import bot.commands.RequirementsCheckResult;
import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class for commands related to role administration. For now thats adding and removing manageable roles
 * ({@link Settings#getAssignableRoles()}) to the bot via commands
 */
public abstract class AdminRoleCommandHandler extends BaseRoleManagementCommandHandler {

    protected abstract Optional<MessageEmbed> manage(Command command, SlashCommandEvent event);

    @Override
    protected Optional<MessageEmbed> execute(Command command, SlashCommandEvent event) {

        try {
            
            return manage(command, event);
            
        } catch(IndexOutOfBoundsException | IllegalArgumentException | IllegalStateException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, "Exception: ", e);
            return Optional.of(Settings.getInstance().createMessage(String.format("Usage: `%s <role ping or id>`", command.getName())));
        } catch (InsufficientPermissionException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, "Exception: ", e);
            return Optional.of(Settings.getInstance().createMessage(e.getMessage()));
        }
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        return super.meetsRequirements(command, event);
    }
}
