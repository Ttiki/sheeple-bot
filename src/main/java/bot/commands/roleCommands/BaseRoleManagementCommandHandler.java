package bot.commands.roleCommands;

import bot.MessageDeleteErrorHandler;
import bot.commands.BaseCommandHandler;
import bot.commands.Command;
import bot.commands.CommandHandler;
import bot.commands.RequirementsCheckResult;
import bot.settings.Settings;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;

import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A base class for command handlers that work on roles. Provides generally needed requirement checks like
 * check for correct channel (role-requests channel).
 * Also provides some basic error handling.
 *
 * @see {@link AdminRoleCommandHandler}, {@link RoleRequestCommandHandler}
 */
public abstract class BaseRoleManagementCommandHandler extends BaseCommandHandler {

    protected BaseRoleManagementCommandHandler() {  }

    /**
     * Any sub class that extends this class should implement this method to actually execute the command
     * @param event The event for the original message
     * @param command the command to be executed
     * @return An optional {@link MessageEmbed} if there is a response to be sent back, {@link Optional#empty()} else
     */
    protected abstract Optional<MessageEmbed> execute(Command command, SlashCommandEvent event);

    /**
     * Implements the {@link CommandHandler} interface
     * @param event The event for the original message
     * @param command the command to be executed
     * @return An optional {@link MessageEmbed} if there is a response to be sent back, {@link Optional#empty()} else
     */
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {
        //make sure the message is from a server
        if(!event.isFromGuild()) {
            event.getHook().deleteOriginal().queue();
           return Optional.empty();
        }

        Optional<MessageEmbed> result = Optional.empty();
        try {
           result = execute(command, event);
        } catch (NullPointerException | InsufficientPermissionException | IllegalArgumentException e) {
           Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception: ");
           event.getTextChannel().sendMessageEmbeds(Settings.getInstance().createMessage(e.getMessage()))
                   .queue(r -> r.delete().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                           null, new MessageDeleteErrorHandler()));
           event.replyEmbeds(Settings.getInstance().createMessage("An error occured: " + e + "\nPlease check the logs")).queue();
           return Optional.empty();
        }
        return result;
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        String roleRequestChannelId = Settings.getInstance().getRoleChannelID();
        String invocationChannelId = event.getChannel().getId(); //channel where the invocation came from
        Member caller = event.getMember();

        try {
            //check for correct channel
            if(invocationChannelId.equals(roleRequestChannelId) || caller.hasPermission(Permission.MANAGE_ROLES)) {
                return super.meetsRequirements(command, event);
            }
            else {
                return RequirementsCheckResult.WRONG_CHANNEL;
            }
        } catch(IllegalArgumentException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, "Exception: ", e);
            e.printStackTrace();
            event.getChannel().sendMessageEmbeds(Settings.getInstance().createMessage(e.getMessage())).queue();
            return RequirementsCheckResult.ERROR;
        }
    }
}
