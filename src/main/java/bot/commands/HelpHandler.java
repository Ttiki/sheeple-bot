package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class HelpHandler extends BaseCommandHandler{
    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {

        Settings settings = Settings.getInstance();

        List<String> piCommands = new ArrayList<>();
        List<String> serverManagementCommands = new ArrayList<>();
        List<String> miscCommands = new ArrayList<>();
        List<String> botManagementCommands = new ArrayList<>();

        for(Command cmd : settings.getCommands().values()) {
            switch (cmd.getCategory()) {

                case PI:
                    piCommands.add(cmd.toString());
                    break;

                case SERVER_MANAGEMENT:
                    serverManagementCommands.add(String.format(cmd.toString(), "<#" + settings.getRoleChannelID() + ">"));
                    break;

                case BOT_MANAGEMENT:
                    botManagementCommands.add(cmd.toString());
                    break;

                default:
                    miscCommands.add(cmd.toString());
            }
        }

        piCommands.sort(Comparator.naturalOrder());
        serverManagementCommands.sort(Comparator.naturalOrder());
        miscCommands.sort(Comparator.naturalOrder());
        botManagementCommands.sort(Comparator.naturalOrder());

        String piCommandsString = String.join("\n", piCommands);
        String serverManagementCommandsString = String.join("\n", serverManagementCommands);
        String miscCommandsString = String.join("\n", miscCommands);
        String botManagementCommandsString = String.join("\n", botManagementCommands);

        MessageEmbed.Field piField = new MessageEmbed.Field(CommandCategories.PI.getCategoryName(), piCommandsString, true);
        MessageEmbed.Field serverManagementField = new MessageEmbed.Field(CommandCategories.SERVER_MANAGEMENT.getCategoryName(),serverManagementCommandsString, true);
        MessageEmbed.Field miscField = new MessageEmbed.Field(CommandCategories.MISC.getCategoryName(), miscCommandsString, true);
        MessageEmbed.Field botManagementField = new MessageEmbed.Field(CommandCategories.BOT_MANAGEMENT.getCategoryName(), botManagementCommandsString, true);


        MessageEmbed msg = Settings.getInstance().createMessage("", String.format("Version %s", Settings.BOT_VERSION),
                miscField,
                new MessageEmbed.Field("","",false),
                serverManagementField,
                new MessageEmbed.Field("","",false),
                piField,
//                new MessageEmbed.Field("", "", false),
                botManagementField);

        return Optional.of(msg);
    }
}
