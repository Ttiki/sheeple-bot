package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.util.Optional;

public class SheepleHandler extends BaseCommandHandler {
    private static final String xkcdURL = "https://xkcd.com/1013/";
    private static final String xkcdPNG = "https://imgs.xkcd.com/comics/wake_up_sheeple.png";

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {
        return Optional.of(Settings.getInstance().createImageMessage("Link: " + xkcdURL, xkcdPNG));
    }

}
