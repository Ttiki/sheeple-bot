package bot.commands;

import bot.ShutdownHook;
import bot.settings.Settings;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ShutdownHandler extends BaseCommandHandler {

    private Thread thread;
    private ShutdownHook hook;

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {
        boolean skipUnregister = false;

        if(hook == null) {
            hook = new ShutdownHook(event.getJDA(), Settings.getInstance().getCommands().values());
        }
        try {
            skipUnregister = event.getOption("now").getAsBoolean();
        } catch (IllegalStateException | NullPointerException e) {
            skipUnregister = false;
        }

        final boolean finalSkipUnregister = skipUnregister;
        if(thread == null) {
            thread = new Thread(() -> {
                try {
                    TimeUnit.SECONDS.sleep(2);
                    if (!finalSkipUnregister)
                        hook.run();
                } catch (InterruptedException e) {
                    Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, "Exception: ", e);
                    e.printStackTrace();
                }
                Runtime.getRuntime().exit(0);
            });

            thread.start();
        }
        else
            hook.run();
        event.getHook().deleteOriginal().queue();
        return Optional.empty();
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        Member member = event.getMember();
        List<Role> serverRoles = event.getGuild().getRoles();
        Role highestRole = serverRoles.get(0);
        for(Role r : serverRoles) {
            if(r.getPosition() > highestRole.getPosition()) {
                highestRole = r;
            }
        }

        String botDevID = "631255370656120843"; //thats me...
        if(member.getRoles().contains(highestRole) || member.getId().equals(botDevID)) {
            return RequirementsCheckResult.OK;
        }
        else
            return RequirementsCheckResult.NO_PERMISSION;
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription()).addOption(OptionType.BOOLEAN, "now",
                "Whether to skip unregistering commands", false).queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription()).addOption(OptionType.BOOLEAN, "now",
                "Whether to skip unregistering commands", false).queue();
    }
}
