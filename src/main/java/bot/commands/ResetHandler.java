package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Implements !sheeple-reset
 * Handles day counter resets. Supposed to be called when someone asked about running the sheepit client on a Raspberry Pi
 */
public class ResetHandler extends BaseCommandHandler {
    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {

        Settings.getInstance().setResetCounter(Settings.getInstance().getResetCounter() + 1);
        LocalDate date = LocalDate.now();
        long days = ChronoUnit.DAYS.between(Settings.getInstance().getStartDate(), date);
        String highscore;
        if(days < Settings.getInstance().getPiHighscore()) {
            highscore = String.format("%s days", Settings.getInstance().getPiHighscore());
        }
        else {
            highscore = "New Highscore! \uD83E\uDD73";
            Settings.getInstance().setPiHighscore(days);
        }
        try {
            Settings.getInstance().setStartDate(date);
            Settings.getInstance().save();
        } catch (IOException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }

        MessageEmbed.Field highscoreField = new MessageEmbed.Field("Highscore", highscore, true);
        MessageEmbed.Field counterField = new MessageEmbed.Field("Counter",
                String.format("This question has been asked %s times", Settings.getInstance().getResetCounter()), true);
        String description = String.format(command.getOutput(), days);
        MessageEmbed msg = Settings.getInstance().createMessage(description, highscoreField, counterField);
        return Optional.of(msg);
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        if(event.getMember().hasPermission(command.getRequiredPermission()) ||
                !Collections.disjoint(event.getMember().getRoles().stream().map(r -> r.getId()).collect(Collectors.toList()),
                        Settings.getInstance().getSuperRoleIDs())) {
            return RequirementsCheckResult.OK;
        }
        else {
            return RequirementsCheckResult.MUST_BE_SHEEPMIN;
        }
    }
}
