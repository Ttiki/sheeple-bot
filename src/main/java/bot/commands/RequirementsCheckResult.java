package bot.commands;

public enum RequirementsCheckResult {
    NO_PERMISSION, MUST_BE_SHEEPMIN, WRONG_CHANNEL, OK, ERROR, IGNORE, INVALID_ARGUMENT;
}
