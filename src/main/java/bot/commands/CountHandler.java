package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

/**
 * Implements !sheeple-count
 * Displays the current day count since the last reset
 */
public class CountHandler extends BaseCommandHandler {

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {

        long days = ChronoUnit.DAYS.between(Settings.getInstance().getStartDate(), LocalDate.now());
        MessageEmbed.Field highscoreField = new MessageEmbed.Field("Highscore", String.format("%s days",
                Settings.getInstance().getPiHighscore()), true);
        String description = String.format(command.getOutput(), days);
        return Optional.of(Settings.getInstance().createMessage(description, highscoreField));
    }
}
