package bot.commands;

import bot.commands.roleCommands.*;
import lombok.Getter;
import net.dv8tion.jda.api.Permission;

@Getter
public enum DefaultCommands {

    RESET("sheeple-reset", "It's been **%d** days since someone asked about running SheepIt on a RaspberryPi.\nCounter reset to 0\n\nSheepit only supports official builds " +
            "of Blender, of which there are currently none for ARM Processors. Apart from that, the Pi is " +
            "too weak to render most frames in under 2 hours. With a power rating of 4%%, a normal frame " +
            "(30 minutes on the reference machine) would take 12,5h.",
            "Reset days counter",
            CommandCategories.PI,
            Permission.MANAGE_CHANNEL,
            new ResetHandler(),
            true,
            false),

    COUNT("sheeple-count",
            "It's been **%d** days since someone asked about running SheepIt on a RaspberryPi.",
            "Count days since last incident",
            CommandCategories.PI,
            Permission.MESSAGE_READ, new CountHandler(),
            true,
            false),

    HELP("sheeple-help",
            "Here is a list of all available commands: \n%s",
            "List all available commands",
            CommandCategories.MISC,
            Permission.MESSAGE_READ,
            new HelpHandler(),
            false,
            false),

    TWIT("sheeple-twit",
            "%s tried to reset the counter. They will be ignored for 30 minutes. The counter remains unchanged",
            "Troll deterrent",
            CommandCategories.MISC,
            Permission.MANAGE_CHANNEL,
            new TwitHandler(),
            true,
            false),

    STATS("sheeple-stats",
            "Webserver Load: %s\nWebserver Overload: %s\nFrames remaining: %s\nFrames rendering: %s\nConnected clients: %s\nActive Projects: %s",
            "Show statistics about the farm",
            CommandCategories.MISC,
            Permission.MESSAGE_READ,
            new StatsHandler(),
            true,
            false),

    JOIN("role-join",
            "%s has been given the following roles: %s",
            "Call in %s to get a role",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MESSAGE_READ,
            new JoinHandlerRequest(),
            true,
            false),

    LEAVE("role-leave",
            "The following roles were removed from %s: %s",
            "Call in %s to remove a role",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MESSAGE_READ,
            new LeaveHandlerRequest(),
            true,
            false),

    ADD("role-add",
            "The following roles were added to the requestable roles: %s",
            "Add a role to the requestable roles",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new AddHandler(),
            true,
            false),

    DEL("role-del",
            "The following roles were removed from the requestable roles: %s",
            "Remove a role from the requestable roles",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new DelHandler(),
            true,
            false),

    LIST("role-list",
            "This is a list of all requestable roles:\n%s\nRun `/" + JOIN.cmd.getName() + " <role name>`",
            "List all requestable roles",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MESSAGE_READ,
            new ListRolesHandler(),
            true,
            false),

    CLEAN_HOUSE("cleanhouse",
            "",
            "Cleans up the %s channel",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_CHANNEL,
            new CleanHouseHandler(),
            true,
            false),

    REGISTER("register",
            "",
            "Register as a member",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MESSAGE_WRITE,
            new RegistrationHandler(),
            true,
            false
            ),

    SHUTDOWN("shutdown",
            "",
            "Stops the bot",
            CommandCategories.BOT_MANAGEMENT,
            Permission.MANAGE_SERVER,
            new ShutdownHandler(),
            true,
            true),

    ARREST("arrest",
            "",
            "Put a victim into confinement and delete all of their recent messages",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new ArrestHandler(),
            true,
            false),

    WIPE("wipe",
            "",
            "If you can execute it you know what it does",
            CommandCategories.SERVER_MANAGEMENT,
            Permission.MANAGE_ROLES,
            new WipeHandler(),
            true,
            true),

    SHEEPLE("sheeple-sheeple",
            "",
            "Where Sheeple came from (not really)",
            CommandCategories.MISC,
            Permission.MESSAGE_READ,
            new SheepleHandler(),
            true,
            false);


    private Command cmd;

    DefaultCommands(String cmdName,
                    String cmdOutput,
                    String cmdDescription,
                    CommandCategories category,
                    Permission requiredPermission,
                    CommandHandler handler,
                    boolean isDefaultCommand,
                    boolean isEphemeral) {
        this.cmd = new Command(cmdName, cmdOutput, cmdDescription, category, requiredPermission,
                handler, isDefaultCommand, isEphemeral);
    }
}
