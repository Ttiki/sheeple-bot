package bot.commands;

import bot.BackgroundTaskHandler;
import bot.MessageDeleteErrorHandler;
import bot.settings.Settings;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.exceptions.HierarchyException;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;

import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ArrestHandler extends BaseCommandHandler {
    private static final long RELEVANT_TIMESPAN = 60 * 60 * 1000 * 6L; //only check messages from within the last X ms
//    private static final long RELEVANT_TIMESPAN = 30;
    private static final String optionVictim = "victim";
    private static final String optionReason = "reason";

    private MessageRecorder recorder;
    private JDA api;

    public ArrestHandler() {
        super();
        recorder = new MessageRecorder(new ArrayList<>());
    }

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {
        Member victim = Objects.requireNonNull(event.getOption(optionVictim)).getAsMember();

        Guild guild = event.getGuild();

        try {
            Role mutedRole = guild.getRoleById(Settings.getInstance().getMutedRoleID());
            if(mutedRole == null) {
                throw new IllegalArgumentException("Muted role returned null. Couldnt retrieve. mutedRoleID: " + Settings.getInstance().getMutedRoleID());
            }
            try {
                guild.modifyMemberRoles(victim, mutedRole).queue();
            } catch(InsufficientPermissionException | HierarchyException | IllegalArgumentException e) {
                Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception while trying to arrest member: " + victim);
                return Optional.of(Settings.getInstance().createMessage("Exception while trying to arrest member: " + victim));
            }

            //subtract 6 hours from now
            long unixTime_sixHoursAgo = Instant.now().getEpochSecond() - RELEVANT_TIMESPAN;

            List<String> toBeDeleted = null;

            synchronized (recorder.getMessageRecords()) {
                toBeDeleted = recorder.getMessageRecords().stream()
                    .filter(messageRecord -> messageRecord.memberID == victim.getIdLong()
                            && messageRecord.getUnixTimeStamp() > unixTime_sixHoursAgo
                            && messageRecord.guildID == guild.getIdLong()
                    )
                    .map(messageRecord -> String.valueOf(messageRecord.getMsgID())).collect(Collectors.toList());
            }

            var reasonOption = event.getOption(optionReason);
            if(toBeDeleted.isEmpty()) {
                printVictimInfo(victim, event.getMember(), reasonOption);
                return Optional.of(Settings.getInstance().createMessage(
                        String.format(
                                "%s has been arrested. No messages from within the last 6 hours found that could be deleted",
                                victim.getEffectiveName()
                        )
                ));
            }

            int msgCount = toBeDeleted.size();

            for (TextChannel channel : guild.getTextChannels()) {
                if(msgCount < 2) {
                    channel.deleteMessageById(toBeDeleted.get(0));
                }
                else {
                    channel.deleteMessagesByIds(toBeDeleted).queue(
                            null, new MessageDeleteErrorHandler()
                    );
                }
            }

            synchronized (recorder.getMessageRecords()) {
                for(String msgID : toBeDeleted) {
                    long numericValue = Long.parseLong(msgID);
                    recorder.getMessageRecords().removeIf(messageRecord -> messageRecord.msgID == numericValue);
                }
            }

            MessageEmbed response = Settings.getInstance().createMessage(
                    String.format(
                            "%s has been arrested. Deleting all messages within the last 6 hours (%d)",
                            victim.getEffectiveName(), msgCount
                    )
            );

            printVictimInfo(victim, event.getMember(), reasonOption);
            return Optional.of(response);
        } catch(IllegalArgumentException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Cant find Muted role?: ");
            return Optional.of(Settings.getInstance().createMessage("Can't find Muted role: " + e.getMessage()));
        }
    }

    private void printVictimInfo(Member victim, Member caller, OptionMapping reasonOption) {
        TextChannel reportChannel = api.getTextChannelById(Settings.getInstance().getVictimReportChannelID());
        if(reportChannel == null) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING,
                    () -> String.format("Couldnt find channel to send the victim report to.%nChannelID: %s", Settings.getInstance().getVictimReportChannelID()));
            return;
        }

        MessageEmbed.Field reasonField = null;
        if(reasonOption != null) {
            reasonField = new MessageEmbed.Field("Arrest reason: ", reasonOption.getAsString(), true);
        }

        MessageEmbed.Field userField = new MessageEmbed.Field("Victim: ", String.format("%s | %s | %s", victim.getAsMention(), victim.getEffectiveName(), victim.getId()), true);
        MessageEmbed.Field serverJoinField = new MessageEmbed.Field("Server Join: ", victim.getTimeJoined().format(DateTimeFormatter.RFC_1123_DATE_TIME), false);
        MessageEmbed.Field discordJoinField = new MessageEmbed.Field("Discord Join: ", victim.getUser().getTimeCreated().format(DateTimeFormatter.RFC_1123_DATE_TIME), true);
        String avatar = victim.getUser().getEffectiveAvatarUrl();
        String footer = String.format("Arrested by %s", caller.getUser().getAsTag());

        MessageEmbed.Field[] fields = new MessageEmbed.Field[4];
        fields[0] = userField;
        fields[1] = serverJoinField;
        fields[2] = discordJoinField;
        if(reasonField != null) fields[3] = reasonField;

        MessageEmbed report = Settings.getInstance().createMessage("", "Inmate record", avatar, footer, fields);
        reportChannel.sendMessageEmbeds(report).queue();
    }

    @Getter
    public class MessageRecorder extends ListenerAdapter {

        private final List<MessageRecord> messageRecords;

        /**
         * For background execution of record cleanup
         */
        private TimerTask backgroundTask;

        public MessageRecorder(List<MessageRecord> messageRecords) {
            this.messageRecords = messageRecords;
            this.backgroundTask = new TimerTask() {
                @Override
                public void run() {
                    synchronized (messageRecords) {
                        //slightly incorrect due to the background task interval. effectively we will keep RELEVANT_TIMESPAN + BACKGROUND LOOP INTERVAL
                        long unixTime_oldest_possible = Instant.now().getEpochSecond() - RELEVANT_TIMESPAN;
                        messageRecords.removeIf(
                                msgRecord->
                                        msgRecord.getUnixTimeStamp() < unixTime_oldest_possible);
                    }
                }
            };
            BackgroundTaskHandler.get().addBackgroundTask(backgroundTask, RELEVANT_TIMESPAN, RELEVANT_TIMESPAN);
        }

        @AllArgsConstructor
        @Getter
        private class MessageRecord {
            private final long msgID;
            private final long memberID;
            private final long unixTimeStamp;
            private final long guildID;
        }

        @Override
        public void onMessageReceived(MessageReceivedEvent event) {
            if (event.getAuthor().isBot()) {
                return;
            }

            long unixTimeStamp = Instant.now().getEpochSecond();
            MessageRecord record = new MessageRecord(event.getMessageIdLong(), event.getMember().getIdLong(), unixTimeStamp, event.getGuild().getIdLong());
            synchronized (messageRecords) {
                messageRecords.add(record);
            }
        }
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        //snag away the api reference
        this.api = api;

        if(!api.getRegisteredListeners().contains(recorder)) {
            api.addEventListener(recorder);
        }

        api.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.USER, optionVictim, "The future inmate", true)
                .addOption(OptionType.STRING, optionReason, "A short explanation for the arrest", false)
                .queue();
    }

    public void registerCommand(Command command, Guild guild) {
        //snag away the api reference
        this.api = guild.getJDA();

        if(!api.getRegisteredListeners().contains(recorder)) {
            api.addEventListener(recorder);
        }

        guild.upsertCommand(command.getName(), command.getDescription())
                .addOption(OptionType.USER, optionVictim, "The future inmate", true)
                .addOption(OptionType.STRING, optionReason, "A short explanation for the arrest", false)
                .queue();
    }

    @Override
    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        try {
            Member victim = event.getOption(optionVictim).getAsMember();
            if(victim == null) return RequirementsCheckResult.ERROR;

            if (victim.hasPermission(Permission.ADMINISTRATOR) || victim.hasPermission(command.getRequiredPermission())) {
                event.replyEmbeds(
                                Settings.getInstance()
                                        .createMessage("You cannot arrest a member with the " + command.getRequiredPermission() + " permission"))
                        .queue(m -> m.deleteOriginal().queueAfter(Settings.getInstance().getDeleteDelaySeconds(), TimeUnit.SECONDS,
                                null, new MessageDeleteErrorHandler()));
                return RequirementsCheckResult.IGNORE;
            }

            List<Role> sheepItRoles = Settings.getInstance().getSuperRoleIDs().stream()
                    .map(r -> event.getGuild().getRoleById(r)).collect(Collectors.toList());

            boolean callerHasSheepItRole = !Collections.disjoint(event.getMember().getRoles(), sheepItRoles);
            if(callerHasSheepItRole || super.meetsRequirements(command, event) == RequirementsCheckResult.OK)
                return RequirementsCheckResult.OK;
            else
                return RequirementsCheckResult.MUST_BE_SHEEPMIN;

        } catch (NullPointerException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.SEVERE, e, () -> "Exception while checking requirements: ");
            return RequirementsCheckResult.ERROR;
        }
    }
}
