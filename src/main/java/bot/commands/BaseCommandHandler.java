package bot.commands;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

public abstract class BaseCommandHandler implements CommandHandler {

    public RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event) {
        return event.getMember().hasPermission(command.getRequiredPermission()) ? RequirementsCheckResult.OK : RequirementsCheckResult.NO_PERMISSION;
    }

    @Override
    public void registerCommand(Command command, JDA api) {
        api.upsertCommand(command.getName(), command.getDescription()).queue();
    }

    @Override
    public void registerCommand(Command command, Guild guild) {
        guild.upsertCommand(command.getName(), command.getDescription()).queue();
    }
}
