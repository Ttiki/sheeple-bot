package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Displays statistics about the farm in the chat
 */
public class StatsHandler extends BaseCommandHandler{

    @Override
    public Optional<MessageEmbed> handle(Command command, SlashCommandEvent event) {
        event.deferReply().queue();
        if(Settings.getInstance().getStatsEndpoint().isEmpty()) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, () -> "No endpoint has been specified in the settings. Keyword: `\"statsEndpoint\"`");
            return Optional.of(Settings.getInstance().createMessage("No endpoint has been specified in the settings. Keyword: `\"statsEndpoint\"`"));
        }

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        String output = "";

        InputStream stream = null;
        try {
            URL url = new URL(Settings.getInstance().getStatsEndpoint());
            stream = url.openStream();

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(stream);
            NamedNodeMap attributes = doc.getElementsByTagName("stats").item(0).getAttributes();

            String activeProjects = attributes.getNamedItem("active_projects").getNodeValue();
            String connectedClients = attributes.getNamedItem("connected_clients").getNodeValue();
            String framesRemaining = attributes.getNamedItem("frames_remaining").getNodeValue();
            String framesRendering = attributes.getNamedItem("frames_rendering").getNodeValue();
            String load = attributes.getNamedItem("load").getNodeValue();
            String overload = attributes.getNamedItem("overload").getNodeValue();

            output = String.format(command.getOutput(), load, overload, framesRemaining, framesRendering,
                    connectedClients, activeProjects);
            stream.close();

        } catch (ParserConfigurationException | SAXException | IOException e) {
            Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, e, () -> "Retrieving stats threw an exception: ");
            return Optional.of(Settings.getInstance().createMessage("Retrieving stats threw a " + e.getClass().getName() + ". Is the server available?"));
        }
        finally {
            if(Objects.nonNull(stream)) {
                try {
                    stream.close();
                } catch (IOException e) {
                    Logger.getLogger(Settings.LOGGER_NAME).log(Level.WARNING, e, () -> "Exception while trying to close stream: ");
                }
            }
        }

        return Optional.of(Settings.getInstance().createMessage(output));
    }
}
