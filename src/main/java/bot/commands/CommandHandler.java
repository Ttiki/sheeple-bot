package bot.commands;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

import java.util.Optional;

public interface CommandHandler {
    Optional<MessageEmbed> handle(Command command, SlashCommandEvent event);

    /**
     * Check for the requirements to execute a command. This method will return true if the calling user has the required
     * permission or is a sheepmin
     * @param command the command that is to be executed
     * @param event the slash command event that was sent by the user
     * @return true if the requirements are met, else false
     */
    RequirementsCheckResult meetsRequirements(Command command, SlashCommandEvent event);

    void registerCommand(Command command, JDA api);

    void registerCommand(Command command, Guild guild);
}
