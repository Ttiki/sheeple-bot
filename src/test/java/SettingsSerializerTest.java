import bot.commands.Command;
import bot.commands.CommandCategories;
import bot.settings.Settings;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import net.dv8tion.jda.api.Permission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;

class SettingsSerializerTest {

    @Test
    void serialize() throws IOException {

        Settings settings = Settings.getInstance();

        Command testCommand = new Command("!pi-test", "This is a test", "test description", CommandCategories.MISC,
                Permission.MANAGE_CHANNEL);
        Command testCommand2 = new Command("!pi-lorem", "Ipsum", "test description", CommandCategories.MISC,
                Permission.MESSAGE_READ);
        HashMap<String, Command> map = new HashMap<>();
        map.put(testCommand.getName(), testCommand);
        map.put(testCommand2.getName(), testCommand2);
        settings.setCommands(map);
        settings.setStartDate(LocalDate.parse("2021-02-24"));
        settings.setDiscordToken(null);
        String json = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(settings);

        JsonParser parser;
        parser = new ObjectMapper().createParser(json);
        Assertions.assertNotNull(parser);
        JsonNode node = parser.getCodec().readTree(parser);
        Assertions.assertNotNull(node);

        Assertions.assertEquals(settings.getMessageAuthor(), node.findValue("messageAuthor").textValue());
        Assertions.assertEquals(settings.getDiscordToken(), node.findValue("discordToken").textValue());
        Assertions.assertEquals(settings.getSuperRoleIDs().size(), node.findValue("sheepitAdminRoleIDs").size());
        Assertions.assertEquals(JsonNodeType.ARRAY, node.findValue("sheepitAdminRoleIDs").getNodeType());

        Iterator<JsonNode> jsonIterator = node.findValue("sheepitAdminRoleIDs").iterator();
        Iterator<String> settingsIterator = settings.getSuperRoleIDs().iterator();

        while(jsonIterator.hasNext()) {
            Assertions.assertEquals(settingsIterator.next(), jsonIterator.next().textValue());
        }

        Assertions.assertEquals(settings.getStartDate().toString(), node.findValue("startDate").textValue());
        Assertions.assertEquals(settings.getCommands().size(), node.findValue("commands").size());
//        System.out.println(json);
//        Assertions.assertEquals(expected, json);
        parser.close();
    }
}