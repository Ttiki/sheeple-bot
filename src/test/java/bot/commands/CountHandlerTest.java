package bot.commands;

import bot.settings.Settings;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Optional;

class CountHandlerTest {

    @Mock
    SlashCommandEvent event;

    @Mock
    Command command;

    String output;


    @BeforeEach
    public void setup() throws NoSuchFieldException, IllegalAccessException {
        event = Mockito.mock(SlashCommandEvent.class);
        Member member = Mockito.mock(Member.class);
        Mockito.when(event.getMember()).thenReturn(member);
        Mockito.when(event.isFromGuild()).thenReturn(true);

        Settings settings = Mockito.mock(Settings.class);
        LocalDate date = LocalDate.now().minusDays(10);
        Mockito.when(settings.getStartDate()).thenReturn(date);
        Field field = Settings.class.getDeclaredField("instance");
        field.setAccessible(true);
        field.set(null, settings);

        field = Settings.class.getDeclaredField("defaultCountCommand");
        field.setAccessible(true);
        output = ((Command) field.get(Settings.getInstance())).getOutput();
        command = Mockito.mock(Command.class);
        Mockito.when(command.getOutput()).thenReturn(output);
    }

    @Test
    void handle() {
        CountHandler countHandler = new CountHandler();
        Optional<MessageEmbed> out = countHandler.handle(command, event);
        Assertions.assertTrue(out.isPresent());
        Assertions.assertTrue(String.format(output, 10).equals(out.get()));

    }

}