import bot.commands.Command;
import bot.commands.CommandCategories;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.dv8tion.jda.api.Permission;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class CommandDeserializerTest {

    @Test
    void deserialize() throws JsonProcessingException {
        String json = "{\"name\":\"!pi-lorem\",\"output\":\"Ipsum\",\"requiredPermission\":\"MESSAGE_READ\", \"description\" : \"test description\"}";
        Command compareValue = new Command("!pi-lorem", "Ipsum", "test description", CommandCategories.PI, Permission.MESSAGE_READ);

        Command cmd = new ObjectMapper().readValue(json, Command.class);
        assertNotNull(cmd);
        assertEquals(compareValue.getName(), cmd.getName());
        assertEquals(compareValue.getOutput(), cmd.getOutput());
        assertEquals(compareValue.getRequiredPermission(), cmd.getRequiredPermission());
        assertEquals(compareValue.getDescription(), cmd.getDescription());
    }
}